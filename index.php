<?php

$user = 'user_id';
$pass = 'user_password';
$msg  = rawurlencode(htmlspecialchars($_POST['sms']));
$url  = ('https://smsapi.free-mobile.fr/sendmsg?user='.$user.'&pass='.$pass.'&msg='.$msg.'');

if(($_SERVER["REQUEST_METHOD"] == "POST")) {
    if (isset($_POST['send']) && !empty($_POST['sms'])) {
        $curl = curl_init();
        $fp = fopen($url, 'r');

        curl_setopt($curl, CURLOPT_URL, $fp);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);

        // execute and return string (this should be an empty string '')
        $str = curl_exec($curl);
        
        curl_close($curl);

    } else {
        $error = '<div style="text-align:center" class="col-lg"><em>not working</em></div><br />';
    }
}

?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <form action="" method="POST">
                        <h4 style="text-align:center">SMS Web App</h4>
                        <div class="form-group">
                            <label for="sms"></label>
                            <textarea class="form-control" name="sms" placeholder="sms here" maxlength="255" id="sms" rows="3"></textarea>
                        </div>

                        <?php if (isset($error)) echo '',$error; ?>

                            <button type="submit" class="btn btn-success btn-lg btn-block" value="send" id="send" name="send">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </html>
